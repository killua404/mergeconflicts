# MergeConflicts und Release Branches

## Aufgabe 1
### Vorbereitung
Clone das Repository in einen lokalen Ordner
### Auftrag
Merge den Branch "test" in den Branch "feature".
### Loesung
- [Loesung](./Loesungen.md#auftrag-1)
## Aufgabe 2
### Vorbereitung
1. Erstellen eines neues Remote-Repos bei dem Anbieter deiner Wahl
2. `git checkout main`
3. Lösche den ".git"-Ordner
### Auftrag
Initialisiere git und verbinde es mit dem persönlichen Repository.
### Loesung
- [Loesung](./Loesungen.md#auftrag-2)
## Aufgabe 3
### Vorbereitung
- `git checkout -b feature`
### Auftrag
Vergebe dem feature-Branch die Version "v1.0" mit der Beschreibung "Version 1.0"
### Loesung
- [Loesung](./Loesungen.md#auftrag-3)
