# Loesungen
## Auftrag 1
1. `git checkout feature`
2. `git merge test`
3. Die Datei anpassen
    - "<<<<<<< HEAD", "=======",">>>>>>> branchname" entfernen
    - Die richtige Anpassung uebernehmen und die andere entfernen
4. `git stage .\myfile.txt`
5. `git commit -m "resolved merge conflicts"`
## Auftrag 2
1. `git init -b main`
2. `git add .`
3. `git commit -m "initial commit"`
4. `git remote add origin [your_repo]`
5. `git branch --set-upstream-to=origin/main main`
6. `git fetch`
7. `git pull --allow-unrelated-histories`
8. Behebe die Mergeconflicts wie in Auftrag 1
9. `git push`
## Auftrag 3
1. `git tag -a v1.0 -m "Version 1.0"`
2. `git push origin --tags`